package com.xiaqf.lesson.springboot01.controller;

import com.alibaba.fastjson.JSON;
import com.xiaqf.lesson.springboot01.common.ConfigPropeties;
import com.xiaqf.lesson.springboot01.domain.UserInfo;
import com.xiaqf.lesson.springboot01.service.UserInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by qfxia on 2017/8/4.
 */
@Controller
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private ConfigPropeties configPropeties;

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/")
    @ResponseBody
    public String home() {
        UserInfo userInfo = userInfoService.selectByPk(1);
        stringRedisTemplate.opsForValue().set(userInfo.getAccount(),JSON.toJSONString(userInfo));

        String user = stringRedisTemplate.opsForValue().get(userInfo.getAccount());


        LOGGER.info("this is a info log");
        LOGGER.debug("this is a debug log");
        LOGGER.warn("this is a warn log");
        LOGGER.error("this is a error log");

        return JSON.toJSONString(user);
    }
}

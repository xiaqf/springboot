package com.xiaqf.lesson.springboot01.domain;

import java.util.Date;

public class UserInfo {
	
	/**
	 * 主键
	 */
    private Integer id;
    
	/**
	 * 用户登录账号
	 */
    private String account;
    
	/**
	 * 用户昵称
	 */
    private String name;
    
	/**
	 * 用户密码（加密后）
	 */
    private String password;
    
	/**
	 * 用户手机号
	 */
    private String mobile;
    
	/**
	 * 用户邮箱
	 */
    private String email;
    
	/**
	 * 用户头像
	 */
    private String avatarUrl;
    
	/**
	 * 是否锁定
	 */
    private String locked;
    
	/**
	 * 用户性别
	 */
    private String sex;
    
	/**
	 * 用户生日
	 */
    private Date birthDate;
    
	/**
	 * 创建时间
	 */
    private Date createDate;
    
	/**
	 * 最近一次登录时间
	 */
    private Date lastLogindate;
    
	/**
	 * 上次锁定时间
	 */
    private Date lastLockdate;

	/**
	 * 用户简介
	 */
    private String description;
    
	/**
	 * 更新人
	 */
    private String updateBy;
    
	/**
	 * 更新时间
	 */
    private Date updateDate;
    

	public Integer getId() {
		return this.id;
	}
    
    public void setId(Integer id) {
		this.id = id;
	}
    
	public String getAccount() {
		return this.account;
	}
    
    public void setAccount(String account) {
		this.account = account;
	}
    
	public String getName() {
		return this.name;
	}
    
    public void setName(String name) {
		this.name = name;
	}
    
	public String getPassword() {
		return this.password;
	}
    
    public void setPassword(String password) {
		this.password = password;
	}
    
	public String getMobile() {
		return this.mobile;
	}
    
    public void setMobile(String mobile) {
		this.mobile = mobile;
	}
    
	public String getEmail() {
		return this.email;
	}
    
    public void setEmail(String email) {
		this.email = email;
	}
    
	public String getAvatarUrl() {
		return this.avatarUrl;
	}
    
    public void setAvatarUrl(String avatarUrl) {
		this.avatarUrl = avatarUrl;
	}
    
	public String getLocked() {
		return this.locked;
	}
    
    public void setLocked(String locked) {
		this.locked = locked;
	}
    
	public String getSex() {
		return this.sex;
	}
    
    public void setSex(String sex) {
		this.sex = sex;
	}
    
	public Date getBirthDate() {
		return this.birthDate;
	}
    
    public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
    
	public Date getCreateDate() {
		return this.createDate;
	}
    
    public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
    
	public Date getLastLogindate() {
		return this.lastLogindate;
	}
    
    public void setLastLogindate(Date lastLogindate) {
		this.lastLogindate = lastLogindate;
	}
    
	public Date getLastLockdate() {
		return this.lastLockdate;
	}
    
    public void setLastLockdate(Date lastLockdate) {
		this.lastLockdate = lastLockdate;
	}
    
	public String getDescription() {
		return this.description;
	}
    
    public void setDescription(String description) {
		this.description = description;
	}
    
	public String getUpdateBy() {
		return this.updateBy;
	}
    
    public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
    
	public Date getUpdateDate() {
		return this.updateDate;
	}
    
    public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
    

}
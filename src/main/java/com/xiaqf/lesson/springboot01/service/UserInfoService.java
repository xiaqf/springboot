package com.xiaqf.lesson.springboot01.service;



import com.xiaqf.lesson.springboot01.domain.UserInfo;

import java.util.List;

public interface UserInfoService {
	
	void insert(UserInfo userInfo);
	
	void update(UserInfo userInfo);
	
	void updateNullable(UserInfo userInfo);
	
	List<UserInfo> selectAll();
	
	UserInfo selectByPk(Integer pk);
	
	void deleteByPk(Integer pk);
	

}
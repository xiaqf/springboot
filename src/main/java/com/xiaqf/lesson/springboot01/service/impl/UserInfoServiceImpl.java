package com.xiaqf.lesson.springboot01.service.impl;

import java.util.List;

import com.xiaqf.lesson.springboot01.domain.UserInfo;
import com.xiaqf.lesson.springboot01.mapper.UserInfoMapper;
import com.xiaqf.lesson.springboot01.service.UserInfoService;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service("userInfoService")
public class UserInfoServiceImpl implements UserInfoService {
	
	@Autowired
	private UserInfoMapper userInfoMapper;
	
	@Override
	public void insert(UserInfo userInfo) {
		userInfoMapper.insertUserInfo(userInfo);
	}
	
	@Override
	public void update(UserInfo userInfo) {
		userInfoMapper.updateUserInfo(userInfo);
	}
	
	@Override
	public void updateNullable(UserInfo userInfo) {
		userInfoMapper.updateUserInfoNullable(userInfo);
	}
	
	@Override
	public List<UserInfo> selectAll() {
		return userInfoMapper.selectUserInfoAll();
	};
	
	@Override
	public UserInfo selectByPk(Integer pk) {
		return userInfoMapper.selectUserInfoByPk(pk);
	}
		
	@Override
	public void deleteByPk(Integer pk) {
		userInfoMapper.deleteUserInfoByPk(pk);
	}

}
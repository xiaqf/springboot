package com.xiaqf.lesson.springboot01.common;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Created by qfixa on 2017/8/3.
 */
@Configuration
@ConfigurationProperties(prefix = "com.xiaqf.lesson")
@PropertySource("classpath:config.properties")
public class ConfigPropeties {

    private String appId;

    private String appSecret;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}

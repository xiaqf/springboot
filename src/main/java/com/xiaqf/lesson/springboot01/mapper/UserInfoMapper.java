package com.xiaqf.lesson.springboot01.mapper;


import com.xiaqf.lesson.springboot01.domain.UserInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserInfoMapper {
	
	void insertUserInfo(UserInfo userInfo);
	
	void updateUserInfo(UserInfo userInfo);
	
	void updateUserInfoNullable(UserInfo userInfo);
		
	List<UserInfo> selectUserInfoAll();
	
	UserInfo selectUserInfoByPk(Integer pk);
	
	void deleteUserInfoByPk(Integer pk);

}
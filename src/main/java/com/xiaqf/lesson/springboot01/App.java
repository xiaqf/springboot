package com.xiaqf.lesson.springboot01;

import com.xiaqf.lesson.springboot01.common.ConfigPropeties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * Created by qfxia on 2017/8/3.
 */
@SpringBootApplication
@EnableConfigurationProperties({ConfigPropeties.class})
public class App {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(App.class, args);
    }

}
